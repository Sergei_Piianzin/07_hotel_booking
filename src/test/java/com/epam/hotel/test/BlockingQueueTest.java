package com.epam.hotel.test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.epam.hotel.queue.BlockingQueue;

public class BlockingQueueTest {

  @Test(timeout = 1000)
  public void shouldReturnNullIfQueueIsEmpty() {
    BlockingQueue<Integer> queue = new BlockingQueue<>(6);
    Integer result = queue.pull();
    assertNull(result);
  }

  @Test(timeout = 1000)
  public void shouldPushAndPullSameValue() {
    BlockingQueue<Integer> queue = new BlockingQueue<>(6);
    queue.push(25);
    Integer result = queue.pull();
    assertThat(result, is(25));
  }

  @Test
  public void shouldAssignCapacity() {
    BlockingQueue<Integer> queue = new BlockingQueue<>(6);
    int capacity = queue.getCapacity();
    assertThat(capacity, is(6));
  }

  @Test
  public void shouldAssignDefaultCapacity() {
    BlockingQueue<Integer> queue = new BlockingQueue<>(-25);
    int capacity = queue.getCapacity();
    assertThat(capacity, is(6));
  }
}
