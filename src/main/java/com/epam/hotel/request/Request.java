package com.epam.hotel.request;

import java.time.LocalDateTime;

public class Request {
  String client;
  String hotel;
  LocalDateTime date;

  public Request(String client, String hotel, LocalDateTime date) {
    this.client = client;
    this.hotel = hotel;
    this.date = date;
  }

  public String getClient() {
    return client;
  }

  public String getHotel() {
    return hotel;
  }

  @Override
  public String toString() {
    return String.format("%s %s %s", client, hotel, date.toString());
  }
}
