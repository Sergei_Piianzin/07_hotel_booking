package com.epam.hotel.queue;

import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BlockingQueue<T> {
  private final Logger logger = LoggerFactory.getLogger("Queue");
  private static final String CONSUMER_WAIT = "WAIT FOR REQUESTS\n";
  private static final String PRODUCER_WAIT = "WAIT FOR BUFFER CLEAR\n";

  private LinkedList<T> queue;
  private AtomicBoolean consumerWork;
  private int pushCount;
  private int pullCount;
  private int capacity;

  // TODO: Cover this constructor tests. Check capacity
  public BlockingQueue(int capacity) {
    queue = new LinkedList<>();
    consumerWork = new AtomicBoolean(true);
    pushCount = 0;
    pullCount = 0;
    this.capacity = getGoodCapacity(capacity);
  }

  private int getGoodCapacity(int capacity) {
    int defaultCapacity = 6;
    return capacity > 1 ? capacity : defaultCapacity;
  }

  public void push(T t) {
    consumerWork.set(true);
    synchronized (queue) {
      while (queue.size() + 1 > capacity) {
        try {
          logger.debug(PRODUCER_WAIT);
          queue.wait();
        } catch (InterruptedException e) {
        }
      }
      queue.add(t);
      logger.debug(String.format("Queue size: %s%n", queue.size()));
      queue.notifyAll();
      pushCount++;
    }
  }

  public T pull() {
    T t = null;
    synchronized (queue) {
      while (queue.isEmpty() && consumerWork.get()) {
        try {
          queue.wait();
          logger.debug(CONSUMER_WAIT);
        } catch (InterruptedException e) {
        }
      }
      if (!queue.isEmpty()) {
        t = queue.remove();
        logger.debug(String.format("Queue size: %s%n", queue.size()));
        queue.notifyAll();
        pullCount++;
      }
    }
    return t;
  }

  public void endConsumerWork() {
    synchronized (queue) {
      queue.notifyAll();
    }
    consumerWork.set(false);
  }

  public Boolean isConsumerWork() {
    return consumerWork.get();
  }

  public boolean isQueueEmpty() {
    return queue.isEmpty();
  }

  public int getPushCount() {
    return pushCount;
  }

  public int getPullCount() {
    return pullCount;
  }

  public int getCapacity() {
    return capacity;
  }
}
