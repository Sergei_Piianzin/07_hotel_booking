package com.epam.hotel.app;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.epam.hotel.consumer.Consumer;
import com.epam.hotel.producer.Client;
import com.epam.hotel.queue.BlockingQueue;
import com.epam.hotel.request.Request;

/**
 * Create pool of consumers and producers. Instance of BlockingQueue connect them. Producer sent any
 * count requests. There is 15 requests in whole.
 */
public class App {
  public static void main(String[] args) {
    String logLevel = "DEBUG";
    System.setProperty(org.slf4j.impl.SimpleLogger.DEFAULT_LOG_LEVEL_KEY, logLevel);
    App app = new App();
    app.run();
  }

  public void run() {
    ExecutorService pool = Executors.newCachedThreadPool();
    BlockingQueue<Request> queue = new BlockingQueue<>(6);
    int requestsCount = 5;
    for (int i = 0; i < 6; i++) {
      if (i < 3) {
        pool.execute(new Client("producer#" + i, requestsCount, queue));
      }
      pool.execute(new Consumer("booker#" + i, queue));
    }
    pool.shutdown();
    try {
      pool.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS);
    } catch (InterruptedException e) {
    }
    System.out.printf("Done. Push: %d, Pull: %d%n", queue.getPushCount(), queue.getPullCount());
  }
}
