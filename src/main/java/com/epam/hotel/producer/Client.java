package com.epam.hotel.producer;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.hotel.queue.BlockingQueue;
import com.epam.hotel.request.Request;

public class Client implements Runnable {
  private final Logger logger = LoggerFactory.getLogger("Producer");

  private BlockingQueue<Request> queue;
  private String name;
  private int requestsCount;

  public Client(String name, int requestsCount, BlockingQueue<Request> queue) {
    this.name = name;
    this.requestsCount = requestsCount;
    this.queue = queue;
  }

  public void run() {
    for (int i = 0; i < requestsCount; ++i) {
      Request request = new Request(name, "Star hotel", LocalDateTime.now());
      logger.info(String.format("%s: sent \"%s\".%n", name, request.toString()));
      queue.push(request);
    }
    queue.endConsumerWork();
  }

  @Override
  public String toString() {
    return name;
  }
}
