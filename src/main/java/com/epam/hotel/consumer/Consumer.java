package com.epam.hotel.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.hotel.queue.BlockingQueue;
import com.epam.hotel.request.Request;

public class Consumer implements Runnable {
  private final Logger logger = LoggerFactory.getLogger("Consumer");
  private BlockingQueue<Request> queue;
  private String title;
  private static final long workTime = 5000;

  public Consumer(String title, BlockingQueue<Request> queue) {
    this.title = title;
    this.queue = queue;
  }

  public void run() {
    while (queue.isConsumerWork() || !queue.isQueueEmpty()) {
      Request request = queue.pull();
      if (request != null) {
        logger.info(String.format("%s : processed \"%s\".%n", title, request.toString()));
        try {
          Thread.currentThread().sleep(workTime);
        } catch (InterruptedException e) {
        }
      }
    }
  }
}
